package com.lawrenzsample.didemo.services;

public interface GreetingService {

    String sayGreeting();
}
